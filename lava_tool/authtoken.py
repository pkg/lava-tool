# Copyright (C) 2011 Linaro Limited
#
# Author: Michael Hudson-Doyle <michael.hudson@linaro.org>
#
# This file is part of lava-tool.
#
# lava-tool is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# lava-tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with lava-tool.  If not, see <http://www.gnu.org/licenses/>.

import base64
import errno
import ConfigParser as configparser
import requests
import urllib
import urllib2
import urlparse
import os
import sys
import xmlrpclib

from lava_tool.interface import LavaCommandError


DEFAULT_KEYRING_FILE = "%s/.local/share/lava-tool/keyring.cfg" % (
    os.path.expanduser("~"))
USERNAME_OPTION_DEFAULT = "default-user"
USERNAME_OPTION_SHORT = "short-"
ENDPOINT_OPTION_SHORT = "endpoint-shortcut"
CONFIG_OPTIONS = [
    USERNAME_OPTION_DEFAULT,
    USERNAME_OPTION_SHORT,
    ENDPOINT_OPTION_SHORT
]


class UsernameNotFoundError(Exception):
    """ Raise when username is not provided and cannot be resolved. """


class EndpointNotFoundError(Exception):
    """ Raise when endpoint is not provided and cannot be resolved. """


class DuplicateEndpointShortcutError(Exception):
    """ Raise when endpoint shortcut is duplicated. """


class DuplicateUserShortcutError(Exception):
    """ Raise when username shortcut is duplicated. """


class ConfigOptionsNotProvidedError(Exception):
    """ Raise when auth config options are not provided. """


def add_scheme(uri, scheme="http"):
    if '://' not in uri:
        uri = '%s://%s' % (scheme, uri)
    return uri


def normalize_xmlrpc_url(uri):
    if not uri.endswith('/'):
        uri += '/'
    if not uri.endswith('/RPC2/'):
        uri += 'RPC2/'
    return uri


def check_uri_for_https(non_secure_uri):
    secure_uri = "%ss:%s" % (
        urllib.splittype(non_secure_uri)[0],
        urllib.splittype(non_secure_uri)[1])
    try:
        res = requests.head(secure_uri, allow_redirects=True, timeout=15)
        if res.status_code != requests.codes.OK:
            return False
    except:
        return False

    return True


def split_xmlrpc_url(url):
    # Split url into username, token and userless_uri.
    # Mind the shortcuts (i.e. non-standard urls and no-scheme urls).
    parsed_host = urlparse.urlparse(url)
    username = None
    token = None
    if parsed_host.netloc:  # has scheme
        host = parsed_host.netloc

        auth, host = urllib.splituser(host)
        if auth:
            username, token = urllib.splitpasswd(auth)

        userless_uri = '%s://%s%s' % (
            parsed_host.scheme, host, parsed_host.path)

    else:  # No scheme, means auth user will end up as parsed_host.scheme if
        # token is present.
        if parsed_host.scheme:
            host = "%s:%s" % (parsed_host.scheme, parsed_host.path)
        else:
            host = parsed_host.path
        auth, userless_uri = urllib.splituser(host)
        if auth:
            username, token = urllib.splitpasswd(auth)

    return username, token, userless_uri


def get_server_url(auth_backend, server):
    # Get resolved url of the server based on authentication backend.

    user, token, host = split_xmlrpc_url(server)
    try:
        host, _ = auth_backend.resolve_shortcuts(host, user)
    except EndpointNotFoundError as e:
        raise LavaCommandError(e)
    except UsernameNotFoundError as e:
        raise LavaCommandError(e)

    return host


class AuthBackend(object):

    def add_token(self, username, endpoint_url, token, user_shortcut=None,
                  endpoint_shortcut=None, default_user=False):
        raise NotImplementedError

    def get_token_for_endpoint(self, user, endpoint_url):
        raise NotImplementedError


class KeyringAuthBackend(AuthBackend):

    def __init__(self, config_file=None):
        if not config_file:
            config_file = DEFAULT_KEYRING_FILE
        self.config_file = config_file

        self.config = configparser.RawConfigParser()
        try:
            os.makedirs(os.path.dirname(self.config_file))
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise
        self.config.read(self.config_file)

    def add_token(self, username, endpoint_url, token, user_shortcut=None,
                  endpoint_shortcut=None, default_user=False):

        # update the keyring with the token
        if not self.config.has_section(endpoint_url):
            self.config.add_section(endpoint_url)
        self.config.set(endpoint_url, username, token)

        self._update_auth_config(endpoint_url, username, user_shortcut,
                                 endpoint_shortcut, default_user)

        # save the config back to the file
        with open(self.config_file, 'w') as config_file:
            self.config.write(config_file)

    def get_token_for_endpoint(self, username, endpoint_url):
        # fetch the token
        try:
            endpoint_url, username = self.resolve_shortcuts(endpoint_url,
                                                            username)
            token = self.config.get(endpoint_url, username)
        except (configparser.NoOptionError, configparser.NoSectionError):
            token = None
        return token

    def remove_token(self, username, endpoint_url):

        endpoint_url, username = self.resolve_shortcuts(
            endpoint_url, username)

        if self.config.remove_option(endpoint_url, username):
            # Remove user shortcuts and default user option.
            try:
                self.config.remove_option(endpoint_url, "%s%s" % (
                    USERNAME_OPTION_SHORT, username))
                for item in self.config.items(endpoint_url):
                    if item[0] == USERNAME_OPTION_DEFAULT and \
                       item[1] == username:
                        self.config.remove_option(endpoint_url, item[0])
            except configparser.NoSectionError:  # No shortcut or default, ignore.
                pass
        else:
            raise UsernameNotFoundError("Username not found.")

        # Remove whole section if this was the last token.
        section_empty = True
        for item in self.config.items(endpoint_url):
            if item[0] not in CONFIG_OPTIONS:
                section_empty = False
                break

        if section_empty:
            self.config.remove_section(endpoint_url)

        with open(self.config_file, 'w') as config_file:
            self.config.write(config_file)

    def get_all_tokens_uri(self, endpoint_url):
        items = []
        try:
            endpoint_url, _ = self.resolve_shortcuts(endpoint_url)
            items = self.config.items(endpoint_url)
        except configparser.NoSectionError:
            pass

        return items

    def get_all_tokens(self):
        items = {}

        for section in self.config.sections():
            items[section] = self.config.items(section)

        return items

    def auth_config(self, username, endpoint_url, user_shortcut=None,
                    endpoint_shortcut=None, default_user=False):

        endpoint_url, username = self.resolve_shortcuts(
            endpoint_url, username)

        if default_user or user_shortcut:
            if not username:
                raise ConfigOptionsNotProvidedError("You must provide username if default user and or user shortcut is to be updated.")
        self._update_auth_config(endpoint_url, username, user_shortcut,
                                 endpoint_shortcut, default_user)

        # save the config back to the file
        with open(self.config_file, 'w') as config_file:
            self.config.write(config_file)

    def resolve_shortcuts(self, endpoint_url, username=None):
        # Return endpoint_url based on shortcuts/defaults.
        target_section = None
        normalized_url = normalize_xmlrpc_url(endpoint_url)
        if self.config.has_section(normalized_url):
            target_section = normalized_url
        else:
            # Find correct config section if endpoint_url is shortcut.
            for section in self.config.sections():
                for item in self.config.items(section):
                    if item[0] == ENDPOINT_OPTION_SHORT:
                        if endpoint_url == item[1]:
                            target_section = section
        if not target_section:
            raise EndpointNotFoundError("Endpoint URL not found in the authorization list: %s" % endpoint_url)

        # If username is shortcut, get the long version.
        for item in self.config.items(target_section):
            if item[0].startswith(USERNAME_OPTION_SHORT) and \
               item[1] == username:
                username = item[0].split(USERNAME_OPTION_SHORT)[1]

        # If no username is provided, find default username in the section.
        if not username:
            for item in self.config.items(target_section):
                if item[0] == USERNAME_OPTION_DEFAULT:
                    username = item[1]

        if not username:
            raise UsernameNotFoundError("Username not found.")

        return target_section, username

    def _update_auth_config(self, endpoint_url, username, user_shortcut=None,
                            endpoint_shortcut=None, default_user=None):
        if user_shortcut:
            # Check if user shortcut is equal to some of the usernames.
            # If so, raise error.
            for item in self.config.items(endpoint_url):
                if item[0] == user_shortcut or item[1] == user_shortcut:
                    raise DuplicateUserShortcutError("Username shortcut already exists as a username. Please provide a different one.")
            self.config.set(endpoint_url,
                            "%s%s" % (USERNAME_OPTION_SHORT, username),
                            user_shortcut)
        if endpoint_shortcut:
            # Check if endpoint shortcut exists for some other endpoint.
            # If yes, raise error.
            for section in self.config.sections():
                for item in self.config.items(section):
                    if item[0] == ENDPOINT_OPTION_SHORT and \
                       item[1] == endpoint_shortcut:
                        raise DuplicateEndpointShortcutError("Endpoint with provided shortcut cannot be duplicated. Please provide a different one.")
            self.config.set(endpoint_url, ENDPOINT_OPTION_SHORT,
                            endpoint_shortcut)
        if default_user:
            self.config.set(endpoint_url, USERNAME_OPTION_DEFAULT,
                            username)


class MemoryAuthBackend(AuthBackend):

    def __init__(self, user_endpoint_token_list):
        self._tokens = {}
        for user, endpoint, token in user_endpoint_token_list:
            self._tokens[(user, endpoint)] = token

    def add_token(self, username, endpoint_url, token, user_shortcut=None,
                  endpoint_shortcut=None, default_user=False):
        self._tokens[(username, endpoint_url)] = token

    def get_token_for_endpoint(self, username, endpoint_url):
        return self._tokens.get((username, endpoint_url))

    def resolve_shortcuts(self, endpoint_url, username=None):
        return normalize_xmlrpc_url(endpoint_url), username


class XMLRPCTransport(xmlrpclib.Transport):

    def __init__(self, scheme, auth_backend):
        xmlrpclib.Transport.__init__(self)
        self.scheme = scheme
        self.auth_backend = auth_backend
        self._opener = urllib2.build_opener()
        self.verbose = 0

    def request(self, host, handler, request_body, verbose=0):
        self.verbose = verbose
        request = self.build_http_request(host, handler, request_body)
        try:
            response = self._opener.open(request)
        except urllib2.HTTPError as e:
            raise xmlrpclib.ProtocolError(
                host + handler, e.code, e.msg, e.info())
        except urllib2.URLError as e:
            raise LavaCommandError(e)

        return self.parse_response(response)

    def build_http_request(self, host, handler, request_body):

        if self.scheme:
            host = add_scheme(host, self.scheme)
        user, token, host = split_xmlrpc_url(host)

        try:
            host, user = self.auth_backend.resolve_shortcuts(host, user)
        except EndpointNotFoundError as e:
            raise LavaCommandError(e)
        except UsernameNotFoundError as e:
            raise LavaCommandError(e)

        host = normalize_xmlrpc_url(host)
        if token is None:
            token = self.auth_backend.get_token_for_endpoint(user, host)
            if user and token is None:
                raise LavaCommandError("Username provided but no token found.")

        parsed_host = urlparse.urlparse(host)
        host = parsed_host.netloc
        url = parsed_host.scheme + "://" + host + handler

        request = urllib2.Request(url, request_body)

        request.add_header("Content-Type", "text/xml")
        if token:
            auth = base64.b64encode(urllib.unquote(user + ':' + token))
            request.add_header("Authorization", "Basic " + auth)

        return request


class AuthenticatingServerProxy(xmlrpclib.ServerProxy):

    def __init__(self, uri, transport=None, encoding=None, verbose=0,
                 allow_none=0, use_datetime=0, auth_backend=None):
        if transport is None:
            scheme = urllib.splittype(uri)[0]
            transport = XMLRPCTransport(scheme, auth_backend=auth_backend)
        if not transport.scheme or transport.scheme == "http":
            if check_uri_for_https(uri):
                print >> sys.stderr, "Warning: Provided endpoint url supports communication over secure protocol (HTTPS)."
        uri = add_scheme(uri)
        xmlrpclib.ServerProxy.__init__(
            self, uri, transport, encoding, verbose, allow_none, use_datetime)
