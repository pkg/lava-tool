# Copyright (C) 2011 Linaro Limited
#
# Author: Michael Hudson-Doyle <michael.hudson@linaro.org>
#
# This file is part of lava-tool.
#
# lava-tool is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# lava-tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with lava-tool.  If not, see <http://www.gnu.org/licenses/>.

import getpass
import urlparse
import xmlrpclib

from lava_tool.authtoken import (
    AuthenticatingServerProxy,
    KeyringAuthBackend,
    MemoryAuthBackend,
    DuplicateEndpointShortcutError,
    DuplicateUserShortcutError,
    EndpointNotFoundError,
    normalize_xmlrpc_url,
    split_xmlrpc_url,
    USERNAME_OPTION_SHORT,
    CONFIG_OPTIONS
)
from lava_tool.interface import Command, LavaCommandError


class auth_add(Command):

    """
    Add an authentication token.
    """

    def __init__(self, parser, args, auth_backend=None):
        super(auth_add, self).__init__(parser, args)
        if auth_backend is None:
            auth_backend = KeyringAuthBackend()
        self.auth_backend = auth_backend

    @classmethod
    def register_arguments(cls, parser):
        super(auth_add, cls).register_arguments(parser)
        parser.add_argument(
            "HOST",
            help=("Endpoint to add token for, in the form "
                  "scheme://username@host. The username will default to "
                  "the currently logged in user."))
        parser.add_argument(
            "--token-file", default=None,
            help="Read the secret from here rather than prompting for it.")
        parser.add_argument(
            "--no-check", action='store_true',
            help=("By default, a call to the remote server is made to check "
                  "that the added token works before remembering it.  "
                  "Passing this option prevents this check."))
        parser.add_argument(
            "--user-shortcut", default=None,
            help="Shortcut name for the user.")
        parser.add_argument(
            "--endpoint-shortcut", default=None,
            help="Shortcut name for the endpoint.")
        parser.add_argument(
            "--default-user", default=False, action='store_true',
            help="Set this user as default user for the endpoint.")

    def invoke(self):
        uri = normalize_xmlrpc_url(self.args.HOST)
        parsed_host = urlparse.urlparse(uri)

        if parsed_host.username:
            username = parsed_host.username
        else:
            username = getpass.getuser()

        host = parsed_host.hostname
        if parsed_host.port:
            host += ':' + str(parsed_host.port)

        uri = '%s://%s@%s%s' % (
            parsed_host.scheme, username, host, parsed_host.path)

        if self.args.token_file:
            if parsed_host.password:
                raise LavaCommandError(
                    "Token specified in url but --token-file also passed.")
            else:
                try:
                    token_file = open(self.args.token_file)
                except IOError as ex:
                    raise LavaCommandError(
                        "opening %r failed: %s" % (self.args.token_file, ex))
                token = token_file.read().strip()
        else:
            if parsed_host.password:
                token = parsed_host.password
            else:
                token = getpass.getpass("Paste token for %s: " % uri)

        userless_uri = '%s://%s%s' % (
            parsed_host.scheme, host, parsed_host.path)

        if not self.args.no_check:
            sp = AuthenticatingServerProxy(
                uri, auth_backend=MemoryAuthBackend(
                    [(username, userless_uri, token)]))
            try:
                token_user = sp.system.whoami()
            except xmlrpclib.ProtocolError as ex:
                if ex.errcode == 401:
                    raise LavaCommandError(
                        "Token rejected by server for user %s." % username)
                else:
                    raise
            except xmlrpclib.Fault as ex:
                raise LavaCommandError(
                    "Server reported error during check: %s." % ex)
            if token_user != username:
                raise LavaCommandError(
                    "whoami() returned %s rather than expected %s -- this is "
                    "a bug." % (token_user, username))

        try:
            self.auth_backend.add_token(username, userless_uri, token,
                                        self.args.user_shortcut,
                                        self.args.endpoint_shortcut,
                                        self.args.default_user)
        except DuplicateEndpointShortcutError as e:
            raise LavaCommandError(e)
        except DuplicateUserShortcutError as e:
            raise LavaCommandError(e)

        print 'Token added successfully for user %s.' % username


class auth_list(Command):

    """
    List all authentication tokens.
    """

    def __init__(self, parser, args, auth_backend=None):
        super(auth_list, self).__init__(parser, args)
        if auth_backend is None:
            auth_backend = KeyringAuthBackend()
        self.auth_backend = auth_backend

    @classmethod
    def register_arguments(cls, parser):
        super(auth_list, cls).register_arguments(parser)
        parser.add_argument(
            "HOST", nargs="?", default=None,
            help=("Endpoint to list tokens for, in the form "
                  "scheme://host."))

    def invoke(self):
        if self.args.HOST:
            username, _, userless_uri = split_xmlrpc_url(self.args.HOST)
            try:
                items = self.auth_backend.get_all_tokens_uri(userless_uri)
            except EndpointNotFoundError as e:
                raise LavaCommandError(e)

            # Format and print everything, including shortcuts and defaults.
            self._print_section(self._format_section(items))

        else:
            config = self.auth_backend.get_all_tokens()
            for section in config:
                items = self._format_section(config[section])
                print "Endpoint URL: %s" % section
                self._print_section(items)
                print "------------"

            if not config:
                print "No tokens found"

    def _format_section(self, items):
        section = {}
        section["users"] = {}
        for item in items:
            if item[0] in CONFIG_OPTIONS:
                section[item[0]] = item[1]
            else:
                if item[0].startswith(USERNAME_OPTION_SHORT):
                    username = item[0].split(USERNAME_OPTION_SHORT)[1]
                    section["users"][username] = item[1]
                else:
                    section["users"][item[0]] = None

        return section

    def _print_section(self, items):
        for key in items:
            if key == "users":
                users = []
                for user in items[key]:
                    if items[key][user]:
                        users.append("%s (%s)" % (user,
                                                  items[key][user]))
                    else:
                        users.append(user)
            else:
                print "%s: %s" % (key, items[key])

        print "Tokens found for users: %s" % ", ".join(users)


class auth_remove(Command):

    """
    Remove authentication token.
    """

    def __init__(self, parser, args, auth_backend=None):
        super(auth_remove, self).__init__(parser, args)
        if auth_backend is None:
            auth_backend = KeyringAuthBackend()
        self.auth_backend = auth_backend

    @classmethod
    def register_arguments(cls, parser):
        super(auth_remove, cls).register_arguments(parser)
        parser.add_argument(
            "HOST",
            help=("Endpoint to remove token for, in the form "
                  "scheme://username@host."))

    def invoke(self):
        username, _, userless_uri = split_xmlrpc_url(self.args.HOST)
        # Check for shortcut and then the uri.
        try:
            self.auth_backend.remove_token(username, userless_uri)
        except Exception as e:
            raise LavaCommandError(e)

        print 'Token successfully removed.'


class auth_config(Command):

    """
    Configure shortcuts and defaults.
    """

    def __init__(self, parser, args, auth_backend=None):
        super(auth_config, self).__init__(parser, args)
        if auth_backend is None:
            auth_backend = KeyringAuthBackend()
        self.auth_backend = auth_backend

    @classmethod
    def register_arguments(cls, parser):
        super(auth_config, cls).register_arguments(parser)
        parser.add_argument(
            "HOST",
            help=("Endpoint to add token for, in the form "
                  "scheme://username@host."))
        parser.add_argument(
            "--user-shortcut", default=None,
            help="Shortcut name for the user.")
        parser.add_argument(
            "--endpoint-shortcut", default=None,
            help="Shortcut name for the endpoint.")
        parser.add_argument(
            "--default-user", default=False, action='store_true',
            help="Set this user as default user for the endpoint.")

    def invoke(self):

        username, _, userless_uri = split_xmlrpc_url(self.args.HOST)
        try:
            self.auth_backend.auth_config(
                username, userless_uri, self.args.user_shortcut,
                self.args.endpoint_shortcut, self.args.default_user)
        except Exception as e:
            raise LavaCommandError(e)

        print 'Auth configuration successfully updated on endpoint %s.' % (
            userless_uri)
