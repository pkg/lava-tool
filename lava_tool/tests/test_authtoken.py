# Copyright (C) 2011 Linaro Limited
#
# Author: Michael Hudson-Doyle <michael.hudson@linaro.org>
#
# This file is part of lava-tool.
#
# lava-tool is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# lava-tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with lava-tool.  If not, see <http://www.gnu.org/licenses/>.

"""
Unit tests for the lava_tool.authtoken package
"""

import base64
import os
import StringIO
from unittest import TestCase
import urlparse
import xmlrpclib

from mocker import ARGS, KWARGS, Mocker

from lava_tool.authtoken import (
    AuthenticatingServerProxy,
    XMLRPCTransport,
    KeyringAuthBackend,
    MemoryAuthBackend,
    EndpointNotFoundError,
    UsernameNotFoundError,
    split_xmlrpc_url,
    USERNAME_OPTION_DEFAULT,
    USERNAME_OPTION_SHORT,
    ENDPOINT_OPTION_SHORT
)
from lava_tool.interface import LavaCommandError


class TestAuthenticatingServerProxy(TestCase):

    def auth_headers_for_method_call_on(self, url, auth_backend):
        parsed = urlparse.urlparse(url)

        mocker = Mocker()
        transport = mocker.mock()

        auth_data = []

        def intercept_request(host, handler, request_body, verbose=0):
            actual_transport = XMLRPCTransport(parsed.scheme, auth_backend)
            request = actual_transport.build_http_request(
                host, handler, request_body)
            if (request.has_header('Authorization')):
                auth_data.append(request.get_header('Authorization'))

        response_body = xmlrpclib.dumps((1,), methodresponse=True)
        response = StringIO.StringIO(response_body)
        response.status = 200
        response.__len__ = lambda: len(response_body)

        transport.request(ARGS, KWARGS)
        mocker.call(intercept_request)
        mocker.result(response)

        # Init scheme attribute.
        transport.scheme

        with mocker:
            server_proxy = AuthenticatingServerProxy(
                url, auth_backend=auth_backend, transport=transport)
            server_proxy.method()

        return auth_data

    def user_and_password_from_auth_data(self, auth_data):
        if len(auth_data) != 1:
            self.fail("expected exactly 1 header, got %r" % len(auth_data))
        [value] = auth_data
        if not value.startswith("Basic "):
            self.fail("non-basic auth header found in %r" % auth_data)
        auth = base64.b64decode(value[len("Basic "):])
        if ':' in auth:
            return tuple(auth.split(':', 1))
        else:
            return (auth, None)

    def test_no_user_no_auth(self):
        auth_headers = self.auth_headers_for_method_call_on(
            'http://localhost/RPC2/', MemoryAuthBackend([]))
        self.assertEqual([], auth_headers)

    def test_token_used_for_auth_http(self):
        auth_headers = self.auth_headers_for_method_call_on(
            'http://user@localhost/RPC2/',
            MemoryAuthBackend([('user', 'http://localhost/RPC2/', 'TOKEN')]))
        self.assertEqual(
            ('user', 'TOKEN'),
            self.user_and_password_from_auth_data(auth_headers))

    def test_token_used_for_auth_https(self):
        auth_headers = self.auth_headers_for_method_call_on(
            'https://user@localhost/RPC2/',
            MemoryAuthBackend([('user', 'https://localhost/RPC2/', 'TOKEN')]))
        self.assertEqual(
            ('user', 'TOKEN'),
            self.user_and_password_from_auth_data(auth_headers))

    def test_port_included(self):
        auth_headers = self.auth_headers_for_method_call_on(
            'http://user@localhost:1234/RPC2/',
            MemoryAuthBackend(
                [('user', 'http://localhost:1234/RPC2/', 'TOKEN')]))
        self.assertEqual(
            ('user', 'TOKEN'),
            self.user_and_password_from_auth_data(auth_headers))

    def test_error_when_user_but_no_token(self):
        self.assertRaises(
            LavaCommandError,
            self.auth_headers_for_method_call_on,
            'http://user@localhost/RPC2/',
            MemoryAuthBackend([]))


class TestKeyringAuthBackend(TestCase):

    def setUp(self):
        self.backend = KeyringAuthBackend(
            config_file="lava_tool/tests/test_keyring.cfg")
        self.backend.add_token("testuser@mytest.org",
                               "http://localhost/RPC2/",
                               "token")
        self.endpoint_url = "http://localhost/RPC2/"

    def tearDown(self):
        os.remove(self.backend.config_file)

    def test_add_token_with_shortcuts(self):
        self.backend.add_token("john.doe@mytest.org",
                               self.endpoint_url,
                               "token", "john", "loca", True)
        self.assertTrue(self.backend.config.has_section(self.endpoint_url))
        self.assertEqual(self.backend.config.get(
            self.endpoint_url, "john.doe@mytest.org"), "token")
        self.assertEqual(self.backend.config.get(
            self.endpoint_url, USERNAME_OPTION_DEFAULT), "john.doe@mytest.org")
        self.assertEqual(self.backend.config.get(
            self.endpoint_url, "%s%s" % (
                USERNAME_OPTION_SHORT,
                "john.doe@mytest.org")), "john")
        self.assertEqual(self.backend.config.get(
            self.endpoint_url, ENDPOINT_OPTION_SHORT), "loca")

    def test_get_token_no_endpoint(self):
        self.assertRaises(
            EndpointNotFoundError,
            self.backend.get_token_for_endpoint,
            None,
            "bad-endpoint"
        )

    def test_get_token_for_endpoint_no_user_no_shortcut(self):
        self.assertRaises(
            UsernameNotFoundError,
            self.backend.get_token_for_endpoint,
            None,
            self.endpoint_url
        )

    def test_get_token_for_endpoint_no_user(self):
        self.assertIsNone(self.backend.get_token_for_endpoint(
            "non-existing-user",
            self.endpoint_url
        ))

    def test_get_token_for_endpoint_shortcut(self):
        self.backend.add_token("john.doe@mytest.org",
                               self.endpoint_url,
                               "token", "john", "loca", True)
        self.assertEqual(self.backend.get_token_for_endpoint(
            "john", self.endpoint_url), "token")

    def test_get_token_for_endpoint(self):
        self.assertEqual(self.backend.get_token_for_endpoint(
            "testuser@mytest.org", self.endpoint_url), "token")

    def test_remove_token_shortcut_no_user(self):
        self.backend.add_token("john.doe@mytest.org",
                               self.endpoint_url,
                               "token", "john", "loca", True)
        self.backend.remove_token(None, "loca")
        self.assertFalse(self.backend.config.has_option(
            self.endpoint_url, "john.doe@mytest.org"))

    def test_remove_token_shortcut(self):
        self.backend.add_token("john.doe@mytest.org",
                               self.endpoint_url,
                               "token", "john", "loca", True)
        self.backend.remove_token("john", "loca")
        self.assertTrue(self.backend.config.has_section(self.endpoint_url))
        self.assertFalse(self.backend.config.has_option(
            self.endpoint_url, "john.doe@mytest.org"))
        self.assertFalse(self.backend.config.has_option(
            self.endpoint_url, USERNAME_OPTION_DEFAULT))
        self.assertFalse(self.backend.config.has_option(
            self.endpoint_url,
            "%s%s" % (USERNAME_OPTION_SHORT, "john.doe@mytest.org")))
        self.assertTrue(self.backend.config.has_option(
            self.endpoint_url, ENDPOINT_OPTION_SHORT))

    def test_remove_token(self):
        self.backend.remove_token("testuser@mytest.org",
                                  self.endpoint_url)
        self.assertFalse(self.backend.config.has_section(self.endpoint_url))

    def test_split_xmlrpc_url_short_no_schema(self):
        self.assertEqual((None, None, "staging"), split_xmlrpc_url("staging"))
        self.assertEqual(("admin", None, "staging"),
                         split_xmlrpc_url("admin@staging"))

    def test_split_xmlrpc_url_short(self):
        self.assertEqual((None, None, "http://staging"),
                         split_xmlrpc_url("http://staging"))
        self.assertEqual(("admin", None, "http://staging"),
                         split_xmlrpc_url("http://admin@staging"))

    def test_split_xmlrpc_url_port(self):
        self.assertEqual((None, None, "http://validation.linaro.org:80/"),
                         split_xmlrpc_url("http://validation.linaro.org:80/"))
        self.assertEqual(("admin", None, "http://validation.linaro.org:80/"),
                         split_xmlrpc_url(
                             "http://admin@validation.linaro.org:80/"))

    def test_split_xmlrpc_url_path(self):
        self.assertEqual(
            (None, None, "http://validation.linaro.org/RPC2/"),
            split_xmlrpc_url("http://validation.linaro.org/RPC2/")
        )
        self.assertEqual(("admin", None, "http://validation.linaro.org/RPC2/"),
                         split_xmlrpc_url(
                             "http://admin@validation.linaro.org/RPC2/"))

    def test_split_xmlrpc_url_token(self):
        self.assertEqual(("admin", "token",
                          "http://validation.linaro.org/RPC2/"),
                         split_xmlrpc_url(
                             "http://admin:token@validation.linaro.org/RPC2/"))
        self.assertEqual(("admin", "token", "staging"),
                         split_xmlrpc_url("admin:token@staging"))
