Description
===========

Summary
#######

``lava-tool`` is a command-line tool to interact with LAVA.

Usage
#####

lava-tool [-h] <subcommand> [args]

Optional arguments
##################

  -h, --help            show this help message and exit

Subcommands
###########

Type ``lava-tool <subcommand> -h`` for help on a specific subcommand.

Deprecated V1 commands
######################

The following commands are **deprecated**. Support will remain in ``lava-tool``
for a limited time only and some calls have already been disabled for some LAVA
instances. Once the rest of the upstream V1 support is removed at the end of
2017, the deprecated functions within ``lava-tool`` will also be removed.

data-views, deserialize, get, put, bundles, server-version, query-data-view,
streams, make-stream, pull.

Available subcommands
#####################

job-output
    Get job output from the scheduler

      Usage:
        lava-tool job-output [-h] [--overwrite] [--output OUTPUT] SERVER JOB_ID

      Positional arguments:
        SERVER
          Host to download job output from
        JOB_ID
          Job ID to download output file

      Optional arguments:
        -h, --help            show this help message and exit
        --overwrite           Overwrite files on the local disk
        --output OUTPUT, -o OUTPUT
                              Alternate name of the output file

devices-list
    Get list of devices from the scheduler.
      Usage:
        lava-tool devices-list [-h] SERVER
      Positional arguments:
        SERVER
          Host to query for the list of devices

      Optional arguments:
        -h, --help  show this help message and exit

help
    Show a summary of all available commands

auth-add
    Add an authentication token

      Usage:
        lava-tool auth-add [-h] [--token-file TOKEN_FILE] [--no-check] HOST

      Positional arguments:
        HOST
          Endpoint to add token for, in the form scheme://username@host. The
          username will default to the currently logged in user.

      Optional arguments:
        -h, --help            show this help message and exit
        --token-file TOKEN_FILE
                              Read the secret from here rather than prompting
                              for it.
        --no-check            By default, a call to the remote server is made
                              to check that the added token works before
                              remembering it. Passing this option prevents this
                              check.
        --user-shortcut USER_SHORTCUT
                              Shortcut name for the user.
        --endpoint-shortcut ENDPOINT_SHORTCUT
                              Shortcut name for the endpoint.
        --default-user        Set this user as default user for the endpoint

auth-config
    Modify authentication configuration

      Usage: lava-tool auth-config [-h] [--user-shortcut USER_SHORTCUT]
                             [--endpoint-shortcut ENDPOINT_SHORTCUT]
                             [--default-user]
                             HOST

      Positional arguments:
        HOST
          Endpoint to add token for, in the form scheme://username@host.

      Optional arguments:
        -h, --help            show this help message and exit
        --user-shortcut USER_SHORTCUT
                        Shortcut name for the user.
        --endpoint-shortcut ENDPOINT_SHORTCUT
                        Shortcut name for the endpoint.
        --default-user        Set this user as default user for the endpoint.

auth-list
    List authentication configuration

      Usage: lava-tool auth-list [-h] [HOST]

      Positional arguments:
        HOST        Endpoint to list tokens for, in the form scheme://host.

      Optional arguments:
        -h, --help  show this help message and exit

cancel-job
    Cancel job

      Usage:
        lava-tool cancel-job [-h] SERVER JOB_ID

      Positional arguments:
        SERVER
          Host to cancel job on
        JOB_ID
          Job ID to cancel

      Optional arguments:
        -h, --help            show this help message and exit

resubmit-job
    Resubmit job

      Usage:
        lava-tool resubmit-job [-h] SERVER JOB_ID

      Positional arguments:
        SERVER
          Host to resubmit job on
        JOB_ID
          Job ID to resubmit

      Optional arguments:
        -h, --help            show this help message and exit

version
    Show the ``lava-tool`` client version

      Usage:
        lava-tool version [-h]

      Optional arguments:
        -h, --help            show this help message and exit

submit-job
    Submit a job to lava-scheduler

      Usage:
        lava-tool submit-job [-h] [--block] SERVER JOB_FILE

      Positional arguments:
        SERVER
          Host to resubmit job on
        JOB_FILE
          JOB file with test definition to submit

      Optional arguments:
        -h, --help            show this help message and exit
        --publish-artifacts   Publish all file:/// references within job
                              definition via publishing API and rewrite URL's.
                              Job definition file must contain the 'secrets'
                              section which includes API_USER and API_KEY which
                              are used to authenticate with remote artifacts
                              server.
        --artifacts-url ARTIFACTS_URL
                              Remote server which accepts file uploads via curl.
                              Defaults to https://archive.validation.linaro.org
                              If non-default url is used, user is responsible
                              for setting up the artifacts upload server as
                              well as relevant account and secret.
        --artifacts-path ARTIFACTS_PATH
                              Location on the remote server to which artifacts
                              will be uploaded.
                              Defaults to team/lava.
        --block               Blocks until the job gets executed. Deprecated.
                              This option adds unnecessary load on the server.
                              Please use wait-job-events command instead.

      Experimental commands:
        --experimental-notice	Explain the nature of experimental commands

wait-job-events
    Wait for a job events from lava-publisher. Can also submit a job and wait
    for that particular one.

    Usage:
      lava-tool wait-job-events [-h]
      (--job-definition JOB_DEFINITION | --job-id JOB_ID)
      [--timeout TIMEOUT] [--quiet]
      SERVER

    Positional arguments:
      SERVER
        Host to submit job on and/or listen to job events from.

    Optional arguments:
      -h, --help            show this help message and exit
      --job-definition JOB_DEFINITION
                            Definition file path of the job that will be
                            submitted.
      --job-id JOB_ID       ID of the job to wait for. If this argument is
                            provided, this command will not submit new job.
      --timeout TIMEOUT     Time to wait until the job is finished. Default will
                            wait until manually canceled.
      --publish-artifacts   Publish all file:/// references within job
                            definition via publishing API and rewrite URL's.
                            Job definition file must contain the 'secrets'
                            section which includes API_USER and API_KEY which
                            are used to authenticate with remote artifacts
                            server.
      --artifacts-url ARTIFACTS_URL
                            Remote server which accepts file uploads via curl.
                            Defaults to https://archive.validation.linaro.org
                            If non-default url is used, user is responsible
                            for setting up the artifacts upload server as
                            well as relevant account and secret.
      --artifacts-path ARTIFACTS_PATH
                            Location on the remote server to which artifacts
                            will be uploaded.
                            Defaults to team/lava.
      --quiet, -q           Do not print out interim messages (when job becomes
                            Submitted or Running) but only when the job is
                            finished.

validate-pipeline-devices
    Validate pipeline devices on the specified server. Print output in the form
    of "device_hostname: {'Valid': null} | {'Invalid': message}"

    Usage:
      lava-tool validate-pipeline-devices [-h] [--name NAME] SERVER

    Positional arguments:
      SERVER

    Optional arguments:
      -h, --help   show this help message and exit
      --name NAME  Hostname of the device or device_type name.

compare-device-conf
    Compare device configurations and output a diff.

      Usage:
        lava-tool compare-device-conf [-h] [--wdiff] [--use-stored USE_STORED]
        [--dispatcher-config-dir DISPATCHER_CONFIG_DIR] [CONFIGS [CONFIGS ...]]

      Positional arguments:
        CONFIGS
          List of device config paths, at least one, max two.

      Optional arguments:
        -h, --help            show this help message and exit
        --wdiff, -w           Use wdiff for parsing output
        --use-stored USE_STORED, -u USE_STORED
          Use stored device config with specified device
        --dispatcher-config-dir DISPATCHER_CONFIG_DIR
          Where to find the device_type templates.

get-pipeline-device-config
    Get pipeline device configuration to a local file or stdout.

    Usage:
        lava-tool get-pipeline-device-config [-h] [--overwrite]
        [--output OUTPUT] [--output-to-stdout] SERVER DEVICE_HOSTNAME

    Positional arguments:
      SERVER
        Host to download pipeline device configuration from
      DEVICE_HOSTNAME
        HOSTNAME of the pipeline device for which configuration is required

    Optional arguments:
      -h, --help            show this help message and exit
      --overwrite           Overwrite files on the local disk
      --output OUTPUT, -o OUTPUT
                            Alternate name of the output file
      --stdout              Write output to stdout

device-dictionary
    Update or export device dictionary data as jinja2 data. [Superusers only.]
    Either [--update | -u] or [--export | -e] must be used.
    Wraps the import-device-dictionary and export-device-dictionary XMLRPC API
    calls on the specified server.

    Usage: lava-tool device-dictionary [-h] [--update UPDATE] [--export]
                                       SERVER DEVICE_HOSTNAME

    Positional arguments:
      SERVER                Host to query or update the device dictionary on
      DEVICE_HOSTNAME       DEVICE_HOSTNAME to query or update

    Optional arguments:
      -h, --help            show this help message and exit
      --update UPDATE, -u UPDATE
                            Load a jinja2 file to update the device dictionary
      --export, -e          Export the device dictionary for this device as jinja2

job-results
    Download job results in either YAML or CSV format.

    Usage: lava-tool job-results [-h] [--csv] SERVER JOB_ID

    Positional arguments:
      SERVER      Host from where to download the results.
      JOB_ID      ID of the job to get results for.

    Optional arguments:
      -h, --help  show this help message and exit
      --csv       If this is specified, results will be printed out in CSV format, otherwise in YAML.

test-suite-results
    Download test suite results in either YAML or CSV format.

    Usage: lava-tool test-suite-results [-h] [--csv] SERVER JOB_ID SUITE_NAME

    Positional arguments:
      SERVER      Host from where to download the results.
      JOB_ID      ID of the job to get results from.
      SUITE_NAME  Name of the suite to get results for.

    Optional arguments:
      -h, --help  show this help message and exit
      --csv       If this is specified, results will be printed out in CSV format, otherwise in YAML.

test-case-results
    Download test case results in either YAML or CSV format.

    Usage: lava-tool test-case-results [-h] [--csv] SERVER JOB_ID SUITE_NAME TEST_CASE

    Positional arguments:
      SERVER      Host from where to download the results.
      JOB_ID      ID of the job to get results from.
      SUITE_NAME  Name of the suite to get results from.
      TEST_CASE   Name of the test case to get results for.

    Optional arguments:
      -h, --help  show this help message and exit
      --csv       If this is specified, results will be printed out in CSV format, otherwise in YAML.


LAVA test definitions
#####################

A LAVA Test Definition comprises of two parts:

* the data to setup the test, the job definition.
* the instructions to run inside the test, expressed as a YAML file.

For help on writing job definitions and test definitions, please see
the help available on the LAVA instance you want to use to run the
test.

As a reminder, JSON job submissions relate to the **deprecated** LAVA V1
support which is superseded by LAVA V2 using YAML for the job submission.

The latest information and help on LAVA V2 is available via the staging
instance:

https://staging.validation.linaro.org/static/docs/v2/

Examples
########

``lava-tool`` simplifies the token handling required to interact with one or
more LAVA instances using XML-RPC.

See the lava-tool help on the instance you want to use when running tests for
more information on using lava-tool. For example, if you have LAVA installed
on ``http://localhost``, the lava-tool help will be available at:
http://localhost/static/docs/v2/lava-tool.html#new-features

Make sure you have already logged into the instance and created an
Authentication Token using the web UI.

Alternatively, admins of up to date LAVA instances can generate a token for a
user with the ``lava-server manage tokens add`` command::

 $ lava-tool auth-add
 http://localuser@localhost/RPC2 Paste token for
 http://localuser@localhost/RPC2/: Token added successfully for user localuser.

Now set the user for this authentication as the default user for this endpoint
(localhost)::

 $ lava-tool auth-config --default-user http://localuser@localhost/RPC2
 Auth configuration successfully updated on endpoint http://localhost/RPC2.

Now set a shortcut for http://localhost/RPC2 as local::

 $ lava-tool auth-config --endpoint-shortcut local http://localuser@localhost/RPC2
 Auth configuration successfully updated on endpoint http://localhost/RPC2.

 $ lava-tool auth-list
 Endpoint URL: http://localhost/RPC2/
 endpoint-shortcut: local
 default-user: localuser
 Tokens found for users: localuser

.. note:: For any instance other than ``localhost``, always use ``https`` so
   that your token is protected during transmission.

Bugs and Issues
###############

.. note:: :command:`lava-tool` is intended for user command line interaction.
   For all scripting requirements use XMLRPC support directly. Help on using
   XMLRPC with python is in the API | Available Methods section of the LAVA instance.
   e.g. https://validation.linaro.org/api/help/ Other languages also have XMLRPC support.
