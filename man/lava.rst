Description
===========

Summary
#######

``lava`` is a command-line tool to interact with LAVA. **DEPRECATED**

Usage
#####

lava [-h] <subcommand> [args]

Optional arguments
##################

  -h, --help            show this help message and exit

Subcommands
###########

Type ``lava <subcommand> -h`` for help on a specific subcommand.

Available subcommands
#####################

status
    Retrieves the status of a job.

      Usage:
        lava status [-h] [--non-interactive] [JOB_ID]

      Positional arguments:
        JOB_ID          Prints status information about the provided job id.

      Optional arguments:
        -h, --help      Show this help message and exit
        --non-interactive, -n
                        Do not ask for input parameters.

run
    Runs a job on the local dispatcher.

      Usage: lava run [-h] [--non-interactive] [JOB]

      Positional arguments:
        JOB             The job file to run, or a directory containing a job
                        file. If nothing is passed, it uses the current
                        working directory.

      Optional arguments:
        -h, --help      Show this help message and exit
        --non-interactive, -n
                        Do not ask for input parameters.

script
    LAVA script file handling.

      usage: lava script [-h] {run, submit} ...

      Optional arguments:
        -h, --help    show this help message and exit

      Sub-command to invoke: {run, submit}
        run
          Runs the specified shell script on a local device.
        submit
          Submits the specified shell script to a LAVA server.

init
    Set-ups the base directory structure.

      Usage: lava init [-h] [--non-interactive] [DIR]

      Positional arguments:
        DIR             The name of the directory to initialize. Defaults to
                        current working directory.

      Optional arguments:
        -h, --help      Show this help message and exit
        --non-interactive, -n
                        Do not ask for input parameters.

submit
    Submits a job to LAVA.

      Usage:
        lava submit [-h] [--non-interactive] [JOB_ID]

      Positional arguments:
        JOB             The job file to send, or a directory containing a job
                        file. If nothing is passed, it uses the current
                        working directory.

      Optional arguments:
        -h, --help      Show this help message and exit
        --non-interactive, -n
                        Do not ask for input parameters.

update
    Updates a job file with the correct data.

      Usage:
        lava update [-h] [--non-interactive] [JOB_ID]

      Positional arguments:
        JOB             Automatically updates a job file definition. If
                        nothing is passed, it usesthe current working
                        directory.

      Optional arguments:
        -h, --help      Show this help message and exit
        --non-interactive, -n
                        Do not ask for input parameters.

job
    LAVA job file handling.

      Usage: lava job [-h] {status, new, run, details, submit} ...

      Optional arguments:
        -h, --help            show this help message and exit

      Sub-command to invoke: {status, new, run, details, submit}
        status
          Retrieves the status of a job.
        new
          Creates a new job file.
        run
          Runs the specified job file on the local dispatcher.
        details
          Retrieves the details of a job.
        submit
          Submits the specified job file.

scheduler
    Interact with LAVA Scheduler

      Usage: lava scheduler [-h] {job-output, devices-list, cancel-job,
      resubmit-job, job-details, job-status, get-pipeline-device-config,
      submit-job, jobs-list}

      Optional arguments:
        -h, --help            show this help message and exit

      Sub-command to invoke: {job-output, devices-list, cancel-job, resubmit-job, job-details, job-status, get-pipeline-device-config, submit-job, jobs-list}
        job-output
          Get job output from the scheduler.
        devices-list
          Get list of devices from the scheduler.
        cancel-job
          Cancel job, if exists.
        resubmit-job
          Resubmit job, if exists.
        job-details
          Get job details, if it existed, from the scheduler.
        job-status
          Get job status and bundle sha1, if it existed, from
          the scheduler.
        get-pipeline-device-config
          Get the pipeline device configuration from scheduler
          to a local file or stdout.
        submit-job
          Submit a job to lava-scheduler
        jobs-list
          Get list of running and submitted jobs from the
          scheduler.

testdef
    LAVA test definitions handling.

      Usage: lava testdef [-h] {new, run, submit} ...

      Optional arguments:
        -h, --help        show this help message and exit

      Sub-command to invoke: {new, run ,submit}
        new
          Creates a new test definition file.
        run
          Runs the specified test definition on a local device.
        submit
          Submits the specified test definition to a LAVA server.


details
    Retrieves the details of a job.

      Usage:
        lava details [-h] [--non-interactive] [JOB_ID]

      Positional arguments:
        JOB_ID          Prints information about job with the provided job id.

      Optional arguments:
        -h, --help      Show this help message and exit
        --non-interactive, -n
                        Do not ask for input parameters.
