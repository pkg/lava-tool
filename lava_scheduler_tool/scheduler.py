# Copyright (C) 2015 Linaro Limited
#
# Author: Stevan Radakovic <stevan.radakovic@linaro.org>
#
# This file is part of lava-scheduler-tool.
#
# lava-scheduler-tool is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# lava-scheduler-tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with lava-scheduler-tool.  If not, see <http://www.gnu.org/licenses/>.

import re
import requests
import subprocess
import sys
import urlparse
import yaml


HTTP_DOWNLOAD_TIMEOUT = 15
DEFAULT_ARTIFACTS_URL = "https://archive.validation.linaro.org/artifacts/"
DEFAULT_ARTIFACTS_UPLOAD_PATH = "team/lava/"


def devicedictionary_to_jinja2(data_dict, extends):
    """
    Formats a DeviceDictionary as a jinja2 string dictionary
    Arguments:
    data_dict: the DeviceDictionary.to_dict()
    extends: the name of the jinja2 device_type template file to extend.
    (including file name extension / suffix) which jinja2 will later
    assume to be in the jinja2 device_types folder
    """
    if not isinstance(data_dict, dict):
        return None
    data = u'{%% extends \'%s\' %%}\n' % extends
    for key, value in data_dict.items():
        if key == 'extends':
            continue
        data += u'{%% set %s = \'%s\' %%}\n' % (key, value)
    return data


def jinja2_to_devicedictionary(data_dict):
    """
    Do some string mangling to convert the template to a key value store
    The reverse of lava_scheduler_app.utils.devicedictionary_to_jinja2
    """
    if not isinstance(data_dict, dict):
        return None
    data = {}
    for line in data_dict.replace('{% ', '').replace(' %}', '').split('\n'):
        if line == '':
            continue
        if line.startswith('extends'):
            base = line.replace('extends ', '')
            base = base.replace('"', "'").replace("'", '')
            data['extends'] = base
        if line.startswith('set '):
            key = line.replace('set ', '')
            key = re.sub(' = .*$', '', key)
            value = re.sub('^.* = ', '', line)
            value = value.replace('"', "'").replace("'", '')
            data[key] = value
    return data


def validate_all_items(data):
    if isinstance(data, dict):
        for item in data.values():
            validate_all_items(item)
    elif isinstance(data, list) or isinstance(data, tuple):
        for item in data:
            validate_all_items(item)
    else:
        validate_single_url(data)


def validate_urls(definition):
    try:
        data = yaml.safe_load(definition)
        validate_all_items(data)
    except yaml.YAMLError as exc:
        print >> sys.stderr, "ERROR: Invalid job definition YAML - %s" % exc


def validate_single_url(url):
    urls = re.findall(r'((https?|git|ftps?)://[^\s]+)', str(url))
    for url in urls:
        if url[1] == 'git':
            try:
                subprocess.check_output(['/usr/bin/git', 'ls-remote',
                                         '--exit-code', '-h', url[0]],
                                        stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as exc:
                sys.stderr.write("Warning: %s\n" % exc)
            except OSError as exc:
                sys.stderr.write("Missing git executable: %s\n" % exc)
        else:
            try:
                res = requests.head(url[0], allow_redirects=True,
                                    timeout=HTTP_DOWNLOAD_TIMEOUT)
                if res.status_code != requests.codes.OK:  # pylint: disable=no-member
                    res = requests.get(
                        url[0], allow_redirects=True, stream=True,
                        timeout=HTTP_DOWNLOAD_TIMEOUT)
                    if res.status_code != requests.codes.OK:  # pylint: disable=no-member
                        sys.stderr.write("Warning: Resources not available at '%s'\n" % (url[0]))

            except requests.Timeout:
                sys.stderr.write("Warning: '%s' timed out\n" % (url[0]))
            except requests.ConnectionError as exc:
                sys.stderr.write("Warning: failed to connect to %s\n" % url[0])
            except requests.RequestException as exc:
                sys.stderr.write("Warning: %s\n" % exc)


def publish_artifacts(definition, artifacts_url=DEFAULT_ARTIFACTS_URL,
                      artifacts_upload_path=DEFAULT_ARTIFACTS_UPLOAD_PATH):

    data = yaml.safe_load(definition)
    # Check job definition for relevant data.
    if 'secrets' not in data:
        sys.stderr.write("Warning: Job definition is missing 'secrets' section. Artifacts will not be published.\n")
        return
    if 'API_KEY' not in data["secrets"]:
        sys.stderr.write("Warning: Job definition 'secrets' section is missing 'API_KEY'. Artifacts will not be published.\n")
        return
    if 'API_USER' not in data["secrets"]:
        sys.stderr.write("Warning: Job definition 'secrets' section is missing 'API_USER'. Artifacts will not be published.\n")
        return

    local_urls = re.findall(r'(file:///[^\s]+)', definition)
    # Remove duplicates, don't upload same files twice.
    local_urls = list(set(local_urls))

    for local_url in local_urls:

        files = {
            'path': open(local_url.replace("file://", ""), 'rb'),
        }
        post_data = {
            'token': data["secrets"]["API_KEY"]
        }

        print("Uploading artifact %s" % local_url)

        request = requests.post(
            urlparse.urljoin(artifacts_url, artifacts_upload_path),
            files=files,
            data=post_data)

        if request.status_code != requests.codes.OK:  # pylint: disable=no-member
            sys.stderr.write("File at %s could not be uploaded. Please contact LAVA support team.\n" % local_url)
            continue

        definition = definition.replace(
            local_url,
            urlparse.urljoin(artifacts_url, request.text))

    return definition
