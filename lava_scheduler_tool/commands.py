# Copyright (C) 2010, 2011 Linaro Limited
#
# Author: Michael Hudson-Doyle <michael.hudson@linaro.org>
#
# This file is part of lava-scheduler-tool.
#
# lava-scheduler-tool is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# lava-scheduler-tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with lava-scheduler-tool.  If not, see <http://www.gnu.org/licenses/>.

# pylint: disable=wrong-import-position

import argparse
import difflib
import jinja2
import json
import os
import re
import sys
import signal
import subprocess
import time
import tempfile
import yaml
import zmq

from lava_tool.authtoken import (
    get_server_url,
    AuthenticatingServerProxy,
    KeyringAuthBackend
)
from lava.tool.command import Command, CommandGroup
from lava.tool.errors import CommandError
from lava_scheduler_tool.scheduler import (
    devicedictionary_to_jinja2,
    jinja2_to_devicedictionary,
    publish_artifacts,
    validate_urls,
    DEFAULT_ARTIFACTS_URL,
    DEFAULT_ARTIFACTS_UPLOAD_PATH
)

if sys.version_info < (3, 0):
    import urlparse
    import xmlrpclib
# Enable only once python3 support is ready, then switch over.
# else:
#    import urllib.parse  # pylint: disable=import-error,unused-import,no-name-in-module
#    import xmlrpc.client as xmlrclib  # pylint: disable=import-error,unused-import

# pylint: disable=superfluous-parens,too-few-public-methods,too-many-locals
# pylint: disable=too-many-branches,too-many-statements,too-many-return-statements


class Timeout(object):
    """ Timeout error class with ALARM signal. Accepts time in seconds. """
    class TimeoutError(Exception):
        pass

    def __init__(self, sec):
        self.sec = sec

    def __enter__(self):
        signal.signal(signal.SIGALRM, self.timeout_raise)
        signal.alarm(self.sec)

    def __exit__(self, *args):  # pylint: disable=unused-argument
        signal.alarm(0)

    def timeout_raise(self, *args):  # pylint: disable=unused-argument,no-self-use
        raise Timeout.TimeoutError()


class scheduler(CommandGroup):  # pylint: disable=invalid-name
    """
    Interact with LAVA Scheduler
    """

    namespace = "lava.scheduler.commands"

    def invoke(self):
        pass

    def reparse_arguments(self, parser, raw_args):
        pass


class submit_job(Command):  # pylint: disable=invalid-name
    """
    Submit a job to lava-scheduler
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(submit_job, cls).register_arguments(parser)
        parser.add_argument("SERVER")
        parser.add_argument("JSON_FILE")
        parser.add_argument(
            "--publish-artifacts",
            action="store_true",
            help="Publish all file:/// references within job definition via publishing API and rewrite URL's.")
        parser.add_argument(
            "--artifacts-url",
            default=DEFAULT_ARTIFACTS_URL,
            help="Remote server which accepts file uploads via curl.")
        parser.add_argument(
            "--artifacts-path",
            default=DEFAULT_ARTIFACTS_UPLOAD_PATH,
            help="Location on the remote server to which artifacts will be uploaded.")
        parser.add_argument(
            "--block",
            action="store_true",
            help="Blocks until the job gets executed. Deprecated. This option adds unnecessary load on the server. Please use wait-job-events command instead.")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)
        if not os.path.exists(self.args.JSON_FILE):
            raise CommandError("No such file: %s" % self.args.JSON_FILE)
        with open(self.args.JSON_FILE, 'rb') as stream:
            command_text = stream.read()
        try:
            if self.args.publish_artifacts:
                command_text = publish_artifacts(
                    command_text,
                    self.args.artifacts_url,
                    self.args.artifacts_path)
            validate_urls(command_text)
            job_ids = server.scheduler.submit_job(command_text)
        except xmlrpclib.Fault as exc:
            raise CommandError(str(exc))
        else:
            # Need to get the host in case of shortcuts usage.
            host = get_server_url(auth_backend, self.args.SERVER)
            if isinstance(job_ids, list):
                print("submitted as jobs:")
                for job_id in job_ids:
                    job = server.scheduler.job_details(job_id)
                    if 'absolute_url' in job:
                        print(urlparse.urljoin(host, job["absolute_url"]))
                    else:
                        print(" - %s" % job_id)
            else:
                job = server.scheduler.job_details(job_ids)
                if 'absolute_url' in job:
                    print("submitted as job: %s" % urlparse.urljoin(
                        host, job["absolute_url"]))
                else:
                    print("submitted as job: %s" % job_ids)
                job_ids = [job_ids]

        if self.args.block:
            print('')
            print('This kind of polling is deprecated and will be removed in the next release. Please use "wait-job-events" command.')
            print('')
            print('Waiting for the job to run ')
            print('. = job waiting in the queue')
            print('# = job running')
            print('')
            for job_id in job_ids:
                print(job_id)
                job = {'job_status': 'Unknown'}
                progress = {'Submitted': '.', 'Running': '#'}
                while job['job_status'] in ['Unknown', 'Submitted', 'Running']:
                    job = server.scheduler.job_status(job_id)
                    sys.stdout.write(progress.get(job['job_status'], ''))
                    sys.stdout.flush()
                    time.sleep(10)  # seconds
                print('')
                print('')
                print('Job Status: %s' % job['job_status'])


class resubmit_job(Command):  # pylint: disable=invalid-name

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        parser.add_argument("SERVER")
        parser.add_argument("JOB_ID")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)
        try:
            job_id = server.scheduler.resubmit_job(self.args.JOB_ID)
            # Need to get the host in case of shortcuts usage.
            host = get_server_url(auth_backend, self.args.SERVER)

        except xmlrpclib.Fault as exc:
            raise CommandError(str(exc))
        else:
            job = server.scheduler.job_details(job_id)
            print("resubmitted as job:", urlparse.urljoin(
                host, job["absolute_url"]))


class cancel_job(Command):  # pylint: disable=invalid-name

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        parser.add_argument("SERVER")
        parser.add_argument("JOB_ID")

    def invoke(self):
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        try:
            server.scheduler.cancel_job(self.args.JOB_ID)
        except xmlrpclib.Fault as exc:
            raise CommandError(str(exc))


class job_output(Command):  # pylint: disable=invalid-name
    """
    Get job output from the scheduler.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(job_output, cls).register_arguments(parser)
        parser.add_argument("SERVER")
        parser.add_argument("JOB_ID",
                            help="Job ID to download output file")
        parser.add_argument("--overwrite",
                            action="store_true",
                            help="Overwrite files on the local disk")
        parser.add_argument("--output", "-o",
                            type=argparse.FileType("wb"),
                            default=None,
                            help="Alternate name of the output file")

    def invoke(self):
        if self.args.output is None:
            filename = str(self.args.JOB_ID) + '_output.txt'
            if os.path.exists(filename) and not self.args.overwrite:
                sys.stderr.write("File {filename!r} already exists\n".format(
                    filename=filename))
                sys.stderr.write("You may pass --overwrite to write over it\n")
                return -1
            stream = open(filename, "wb")
        else:
            stream = self.args.output
            filename = self.args.output.name

        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        try:
            stream.write(server.scheduler.job_output(self.args.JOB_ID).data)
            print("Downloaded job output of {0} to file {1!r}".format(
                self.args.JOB_ID, filename))
        except xmlrpclib.Fault as exc:
            sys.stderr.write(exc)
            return -1


class job_status(Command):  # pylint: disable=invalid-name
    """
    Get job status and bundle sha1, if it existed, from the scheduler.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(job_status, cls).register_arguments(parser)
        parser.add_argument("SERVER")
        parser.add_argument("JOB_ID",
                            help="Job ID to check the status")

    def invoke(self):
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        ret_status = server.scheduler.job_status(self.args.JOB_ID)

        print("Job ID: %s\nJob Status: %s\nBundle SHA1: %s" %
              (str(self.args.JOB_ID), ret_status['job_status'],
               ret_status['bundle_sha1']))


class job_details(Command):  # pylint: disable=invalid-name
    """
    Get job details, if it existed, from the scheduler.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(job_details, cls).register_arguments(parser)
        parser.add_argument("SERVER")
        parser.add_argument("JOB_ID",
                            help="Job ID to find the details")

    def invoke(self):
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        ret_details = server.scheduler.job_details(self.args.JOB_ID)

        print("Details of job {0}: \n".format(str(self.args.JOB_ID)))
        for detail in ret_details:
            print("%s: %s" % (detail, ret_details[detail]))


class jobs_list(Command):  # pylint: disable=invalid-name
    """
    Get list of running and submitted jobs from the scheduler.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(jobs_list, cls).register_arguments(parser)
        parser.add_argument("SERVER")

    def invoke(self):
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        try:
            ret_list = server.scheduler.all_jobs()
        except xmlrpclib.Fault as exc:
            raise CommandError(str(exc))

        print(json.dumps(ret_list))


class devices_list(Command):  # pylint: disable=invalid-name
    """
    Get list of devices from the scheduler.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(devices_list, cls).register_arguments(parser)
        parser.add_argument("SERVER")

    def invoke(self):
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        all_devices = server.scheduler.all_devices()

        print(all_devices)


class get_pipeline_device_config(Command):  # pylint: disable=invalid-name
    """
    Get the pipeline device configuration from scheduler to a local file
    or stdout.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(get_pipeline_device_config, cls).register_arguments(parser)
        parser.add_argument("SERVER",
                            help="Host to download pipeline device config from")
        parser.add_argument("DEVICE_HOSTNAME",
                            help="DEVICE_HOSTNAME to download config file")
        parser.add_argument("--overwrite",
                            action="store_true",
                            help="Overwrite files on the local disk")
        parser.add_argument("--output", "-o",
                            type=argparse.FileType("wb"),
                            default=None,
                            help="Alternate name of the config file")
        parser.add_argument("--stdout",
                            action="store_true",
                            help="Write output to stdout")

    def invoke(self):
        if self.args.output is None and not self.args.stdout:
            filename = str(self.args.DEVICE_HOSTNAME) + '_config.yaml'
            if os.path.exists(filename) and not self.args.overwrite:
                sys.stderr.write("File {filename!r} already exists\n".format(
                    filename=filename))
                sys.stderr.write("You may pass --overwrite to write over it\n")
                return -1
            stream = open(filename, "wb")
        elif self.args.stdout:
            stream = sys.stdout
            filename = "stdout"
        else:
            stream = self.args.output
            filename = self.args.output.name

        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=KeyringAuthBackend())
        try:
            stream.write(server.scheduler.get_pipeline_device_config(
                self.args.DEVICE_HOSTNAME).data)
            print("Downloaded device config of {0} to file {1!r}".format(
                self.args.DEVICE_HOSTNAME, filename))
        except xmlrpclib.Fault as exc:
            sys.stderr.write(exc)
            return -1


class compare_device_conf(Command):  # pylint: disable=invalid-name
    """
    Compare device config YAML files.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(compare_device_conf, cls).register_arguments(parser)
        parser.add_argument("--wdiff", "-w",
                            action='store_true',
                            help="Use wdiff for parsing output")

        parser.add_argument("--use-stored", "-u",
                            default=None,
                            help="Use stored device config with specified device")
        parser.add_argument("--dispatcher-config-dir",
                            default="/etc/lava-server/dispatcher-config/",
                            help="Where to find the device_type templates.")
        parser.add_argument("CONFIGS",
                            nargs='*',
                            help="List of device config paths, at least one, max two.")

    def invoke(self):

        configs = self.args.CONFIGS

        # Validate number of arguments depending on the options.
        if self.args.use_stored is None:
            if len(self.args.CONFIGS) != 2:
                sys.stderr.write("Please input two arguments with config file paths\n")
                sys.stderr.write("You may use --use-stored with one config file path\n")
                return -1

            for path in self.args.CONFIGS:
                if not os.path.exists(path):
                    sys.stderr.write("File {path!r} does not exist\n".format(
                        path=path))
                    return -1

        else:
            if len(self.args.CONFIGS) != 1:
                sys.stderr.write("Please input one argument with config file path\n")
                sys.stderr.write("You may omit --use-stored and use two config file paths\n")
                return -1

            path = self.args.CONFIGS[0]
            if not os.path.exists(path):
                sys.stderr.write("File {path!r} does not exist\n".format(
                    path=path))
                return -1

            # Run device-dictionary --preview and load it into tmp file.
            args = [
                "lava-server",
                "manage",
                "device-dictionary",
                "--hostname=%s" % self.args.use_stored,
                "--export"
            ]

            _, config_path = tempfile.mkstemp()

            return_code = subprocess.call(
                args,
                stdout=open(config_path, "w")
            )

            if return_code != 0:
                sys.stderr.write("Device config for {device!r} doesn't exist\n".format(device=self.args.use_stored))
                return -1

            configs.append(config_path)

        # Load templates and compare. Current output is classic unified diff.
        device_confs = []
        for path in configs:
            with open(path) as read_file:
                line = read_file.readline()

            if re.search(r'\{%\sextends\s.*%\}', line):
                # First line matches 'extends' regex. Treat it as a template.
                data = self._parse_template(path)
                config = devicedictionary_to_jinja2(
                    data,
                    data['extends']
                )
                string_loader = jinja2.DictLoader({'%s' % path: config})
                type_loader = jinja2.FileSystemLoader([
                    os.path.join(self.args.dispatcher_config_dir,
                                 'device-types')])
                env = jinja2.Environment(
                    loader=jinja2.ChoiceLoader([string_loader, type_loader]),
                    trim_blocks=True)
                template = env.get_template("%s" % path)
                device_configuration = template.render()
                device_confs.append(
                    device_configuration.strip("\n").split("\n"))
            else:
                # 'Extends' not matched. Treat this as a regular config file.
                try:
                    yaml.safe_load(open(path, 'r'))
                except yaml.YAMLError:
                    print("Please provide a valid YAML configuration file.")
                    sys.exit(2)

                device_configuration = []
                with open(path) as read_file:
                    device_configuration = [dline.strip('\n') for dline in read_file.readlines()]

                device_confs.append(device_configuration)

        diff = difflib.unified_diff(device_confs[0], device_confs[1],
                                    fromfile=configs[0],
                                    tofile=configs[1])
        data_input = [iline for iline in diff]

        if self.args.wdiff:
            # Pass diff to wdiff for word diff output.
            tempfile.mkstemp()

            args = ["wdiff", "-d"]

            proc = subprocess.Popen(
                args,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE
            )
            out, _ = proc.communicate("\n".join(data_input))

            if out:
                print(out)

        if not self.args.wdiff:
            for line in data_input:
                print(line)

        if not data_input:
            print("Success. The configuration files are identical.")

        return 0

    def _parse_template(self, device_file):  # pylint: disable=no-self-use

        if not os.path.exists(os.path.realpath(device_file)):
            print("Unable to find file '%s'\n" % device_file)
            sys.exit(2)
        with open(device_file, 'r') as fileh:
            content = fileh.read()
        return jinja2_to_devicedictionary(content)


class device_dictionary(Command):  # pylint: disable=invalid-name
    """
    Update or export device dictionary data as jinja2 data.
    [Superusers only.]
    """

    def invoke(self):
        if self.args.update and self.args.export:
            sys.stderr.write("Please use either update or export.\n")
            return 1
        elif self.args.export:
            hostname = str(self.args.DEVICE_HOSTNAME)
            stream = sys.stdout
            server = AuthenticatingServerProxy(
                self.args.SERVER, auth_backend=KeyringAuthBackend())
            if self.args.export:
                try:
                    stream.write(server.scheduler.export_device_dictionary(hostname).data)
                except xmlrpclib.Fault as exc:
                    sys.stderr.write("%s\n" % str(exc))
                    return -1
        elif self.args.update:
            server = AuthenticatingServerProxy(
                self.args.SERVER, auth_backend=KeyringAuthBackend())
            hostname = str(self.args.DEVICE_HOSTNAME)
            filename = self.args.update
            if not os.path.exists(filename):
                sys.stderr.write("File {filename!r} does not exist\n".format(
                    filename=filename))
                return -1
            with open(filename, "r") as reader:
                data = reader.read()
            print("Updating device dictionary for %s on %s\n" % (hostname, self.args.SERVER))
            try:
                print(server.scheduler.import_device_dictionary(hostname, data))
            except xmlrpclib.Fault as exc:
                sys.stderr.write("%s\n" % str(exc))
                return -1
        else:
            sys.stderr.write("Unrecognised options.\n")
            return 2

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(device_dictionary, cls).register_arguments(parser)
        parser.add_argument("SERVER",
                            help="Host to query or update the device dictionary on")
        parser.add_argument("DEVICE_HOSTNAME",
                            help="DEVICE_HOSTNAME to query or update")
        parser.add_argument("--update", "-u",
                            help="Load a jinja2 file to update the device dictionary")
        parser.add_argument("--export", "-e", action='store_true',
                            help="Export the device dictionary for this device as jinja2")


class wait_job_events(Command):  # pylint: disable=invalid-name
    """
    Wait for job to finish and return job details.
    """

    FINISHED_JOB_STATE = ["Finished"]
    FINISHED_JOB_STATUS = ["Complete", "Incomplete", "Canceled"]
    EVENT_SOCKET_PATTERN_SYMBOL = "*"
    API_UPDATE_VERSION = "2018.1"

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(wait_job_events, cls).register_arguments(parser)
        parser.add_argument("SERVER")
        action = parser.add_mutually_exclusive_group(required=True)
        action.add_argument("--job-definition",
                            help="Definition file path of the job that will be submitted.")
        action.add_argument("--job-id",
                            type=int,
                            help="ID of the job to wait for. If this argument is provided, this command will not submit new job.")
        parser.add_argument("--timeout",
                            type=int,
                            default=0,
                            help="Time to wait until the job is finished. Default will wait until manually canceled.")
        parser.add_argument(
            "--publish-artifacts",
            action="store_true",
            help="Publish all file:/// references within job definition via publishing API and rewrite URL's.")
        parser.add_argument(
            "--artifacts-url",
            default=DEFAULT_ARTIFACTS_URL,
            help="Remote server which accepts file uploads via curl.")
        parser.add_argument(
            "--artifacts-path",
            default=DEFAULT_ARTIFACTS_UPLOAD_PATH,
            help="Location on the remote server to which artifacts will be uploaded.")
        parser.add_argument(
            "--quiet", "-q",
            action="store_true",
            help="Do not print out interim messages (when job becomes Submitted or Running) but only when the job is finished.")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)

        # Need to get the host in case of shortcuts usage.
        host = get_server_url(auth_backend, self.args.SERVER)
        hostname = urlparse.urlparse(host).hostname

        # Support both old and new API versions.
        if server.system.version().split("+")[0] >= self.API_UPDATE_VERSION:
            state_key = "state"
            relevant_finished_states = self.FINISHED_JOB_STATE
        else:
            state_key = "status"
            relevant_finished_states = self.FINISHED_JOB_STATUS

        if self.args.job_id:
            job_ids = [self.args.job_id]
            try:
                job = server.scheduler.job_details(self.args.job_id)

                if job[state_key] in relevant_finished_states:
                    print("Job %s already finished with state %s." % (
                        self.args.job_id, job["health"]))
                    return

            except xmlrpclib.Fault:
                print("Job details were not found for the specified job. "
                      "Please make sure job-id exists or that this job is about to get submitted.")

        else:
            if not os.path.exists(self.args.job_definition):
                raise CommandError(
                    "No such file: %s" % self.args.job_definition)
            try:
                with open(self.args.job_definition, 'rb') as stream:
                    command_text = stream.read()
            except IOError as exc:
                sys.stderr.write("%s\n" % str(exc))
                return -1
            try:
                if self.args.publish_artifacts:
                    command_text = publish_artifacts(
                        command_text,
                        self.args.artifacts_url,
                        self.args.artifacts_path)
                validate_urls(command_text)
                job_ids = server.scheduler.submit_job(command_text)

                if not isinstance(job_ids, list):
                    job_ids = [job_ids]

                print("submitted as job(s):")
                for job_id in job_ids:
                    job = server.scheduler.job_details(job_id)
                    print(urlparse.urljoin(host, job["absolute_url"]))

            except xmlrpclib.Fault as exc:
                raise CommandError(str(exc))

        job_url_template = urlparse.urljoin(
            host, job["absolute_url"].rsplit("/", 1)[0])

        host_pattern = server.scheduler.get_publisher_event_socket()
        event_socket = host_pattern.replace(
            self.EVENT_SOCKET_PATTERN_SYMBOL, hostname, 1)

        try:
            with Timeout(self.args.timeout):
                context = zmq.Context.instance()
                sock = context.socket(zmq.SUB)  # pylint: disable=no-member

                sock.setsockopt(zmq.SUBSCRIBE, b"")  # pylint: disable=no-member
                sock.connect(event_socket)

                print("Now waiting for job events...")
                while True:
                    msg = sock.recv_multipart()
                    try:
                        (topic, uuid, device_type, username, data) = msg[:]  # pylint: disable=unused-variable
                    except IndexError:
                        # Droping invalid message
                        continue

                    data = yaml.safe_load(data)
                    if "job" in data:
                        # Filter out device events.
                        if "submitter" in data:
                            if data["job"] in job_ids:
                                data["job"] = urlparse.urljoin(
                                    job_url_template, str(data["job"]))
                                if data[state_key] in relevant_finished_states:
                                    print(data)
                                    break
                                elif not self.args.quiet:
                                    print(data)

        except zmq.error.ZMQError as exc:
            sys.stderr.write("wait_job_events() error: %s\n" % exc)
            return -1
        except KeyboardInterrupt:
            sys.stderr.write("wait_job_events() cancelled\n")
            return -1
        except Timeout.TimeoutError:
            sys.stderr.write("wait_job_events() timed out after %s seconds.\n" % self.args.timeout)
            try:
                for job_id in job_ids:
                    ret_details = server.scheduler.job_details(job_id)
                    print("Job %s currently has state %s." % (
                        job_id, ret_details["state"]))
                    print("You can check the job details with 'lava-tool job-details' command.")
                return 1
            except xmlrpclib.Fault:
                # Keep silent, message about non-existing job is already
                # printed during first job_details() call.
                return -1

        return 0


class version(Command):

    """
    Show lava-tool client version
    """

    def invoke(self):
        from lava_tool import version
        print "lava-tool client version: {version}".format(
            version=version())


class validate_pipeline_devices(Command):  # pylint: disable=invalid-name
    """
    Validate pipeline devices on the specified server.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(validate_pipeline_devices, cls).register_arguments(parser)
        parser.add_argument("SERVER")
        parser.add_argument("--name",
                            default="",
                            help="Hostname of the device or device_type name.")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)

        try:
            print(server.scheduler.validate_pipeline_devices(
                self.args.name))
        except xmlrpclib.Fault as exc:
            raise CommandError(str(exc))


class job_results(Command):  # pylint: disable=invalid-name
    """
    Get job results as CSV or YAML.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(job_results, cls).register_arguments(parser)
        parser.add_argument("SERVER",
                            help="Host from where to download the results.")
        parser.add_argument("JOB_ID",
                            type=int,
                            help="ID of the job to get results for.")
        parser.add_argument("--csv",
                            action="store_true",
                            help="If this is specified, results will be printed out in CSV format, otherwise in YAML.")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)

        try:
            if self.args.csv:
                print(server.results.get_testjob_results_csv(self.args.JOB_ID))
            else:
                print(server.results.get_testjob_results_yaml(self.args.JOB_ID))
        except xmlrpclib.Fault:
            print("Command did not found specified job. Please make sure JOB_ID exists on the specified SERVER.")


class test_suite_results(Command):  # pylint: disable=invalid-name
    """
    Get test suite results as CSV or YAML.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(test_suite_results, cls).register_arguments(parser)
        parser.add_argument("SERVER",
                            help="Host from where to download the results.")
        parser.add_argument("JOB_ID",
                            type=int,
                            help="ID of the job to get results from.")
        parser.add_argument("SUITE_NAME",
                            help="Name of the suite to get results for.")
        parser.add_argument("--csv",
                            action="store_true",
                            help="If this is specified, results will be printed out in CSV format, otherwise in YAML.")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)

        try:
            if self.args.csv:
                print(server.results.get_testsuite_results_csv(
                    self.args.JOB_ID,
                    self.args.SUITE_NAME))
            else:
                print(server.results.get_testsuite_results_yaml(
                    self.args.JOB_ID,
                    self.args.SUITE_NAME))
        except xmlrpclib.Fault:
            print("Command did not find the specified job or test suite. "
                  "Please make sure JOB_ID and SUITE_NAME exist on the specified SERVER.")


class test_case_results(Command):  # pylint: disable=invalid-name
    """
    Get test case results as CSV or YAML.
    """

    def reparse_arguments(self, parser, raw_args):
        pass

    @classmethod
    def register_arguments(cls, parser):
        super(test_case_results, cls).register_arguments(parser)
        parser.add_argument("SERVER",
                            help="Host from where to download the results.")
        parser.add_argument("JOB_ID",
                            type=int,
                            help="ID of the job to get results from.")
        parser.add_argument("SUITE_NAME",
                            help="Name of the suite to get results from.")
        parser.add_argument("TEST_CASE",
                            help="Name of the test case to get results for.")
        parser.add_argument("--csv",
                            action="store_true",
                            help="If this is specified, results will be printed out in CSV format, otherwise in YAML.")

    def invoke(self):
        auth_backend = KeyringAuthBackend()
        server = AuthenticatingServerProxy(
            self.args.SERVER, auth_backend=auth_backend)

        try:
            if self.args.csv:
                print(server.results.get_testcase_results_csv(
                    self.args.JOB_ID,
                    self.args.SUITE_NAME,
                    self.args.TEST_CASE))
            else:
                print(server.results.get_testcase_results_yaml(
                    self.args.JOB_ID,
                    self.args.SUITE_NAME,
                    self.args.TEST_CASE))
        except xmlrpclib.Fault:
            print("Command did not find the specified job, test suite or test case. "
                  "Please make sure JOB_ID, SUITE_NAME and TEST_CASE exist on the specified SERVER.")
