# Copyright (C) 2013 Linaro Limited
#
# Author: Milo Casagrande <milo.casagrande@linaro.org>
#
# This file is part of lava-tool.
#
# lava-tool is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3
# as published by the Free Software Foundation
#
# lava-tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with lava-tool.  If not, see <http://www.gnu.org/licenses/>.

"""Classes and functions to interact with the lava-dispatcher."""

import random
import string
import os

from lava.tool.errors import CommandError

# Default devices path, has to be joined with the dispatcher path.
DEFAULT_DEVICES_PATH = "devices"


def get_devices():
    """Gets the devices list from the dispatcher.

    :return A list of DeviceConfig.
    """
    try:
        from lava_dispatcher.config import get_devices
        return get_devices()
    except ImportError:
        raise CommandError("Cannot find lava-dispatcher installation.")
